const express = require('express');

const router = express.Router();
const { registerUser, loginUser } = require('./authService');
// const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/register', registerUser);

router.post('/login', loginUser);

module.exports = {
  authRouter: router,
};
