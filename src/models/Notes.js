const mongoose = require('mongoose');

const { Schema } = mongoose;

const noteSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    required: true,
  },
});

const Note = mongoose.model('Notes', noteSchema);
module.exports = Note;
