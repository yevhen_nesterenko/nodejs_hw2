const express = require('express');

const router = express.Router();
const { getUserInfo, deleteUser, updateUser } = require('./usersService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/', authMiddleware, getUserInfo);
router.delete('/', authMiddleware, deleteUser);
router.patch('/', authMiddleware, updateUser);

module.exports = {
  usersRouter: router,
};
