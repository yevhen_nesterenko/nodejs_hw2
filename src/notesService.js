const Note = require('./models/Notes');

const getUserNotes = async (req, res) => {
  const userNotes = await Note.find({ userId: req.user.userId }, '-__v');
  const count = userNotes.length;
  if (!userNotes) {
    return res.status(400).json({
      message: 'This user does not have notes',
    });
  }
  const { offset = 0, limit = count } = req.query;
  const cuttedUserNotes = userNotes.splice(offset, limit);
  return res.json({
    offset: +offset,
    limit: +limit,
    count,
    notes: cuttedUserNotes,
  });
};
// http://localhost:8080/api/notes?offset=0&limit=10
const addUserNote = async (req, res) => {
  const { text } = req.body;
  if (!text) {
    return res.status(400).json({
      message: "Please specify 'text' parameter",
    });
  }
  const note = new Note({
    userId: req.user.userId,
    text,
    createdDate: new Date(),
  });
  const savedNote = await note.save();
  if (!savedNote) {
    throw new Error();
  }
  return res.json({ message: 'Success' });
};

const getUserNote = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const note = await Note.findOne({ _id: id }, '-__v');
    return res.json({
      note,
    });
  } catch (err) {
    return res.status(400).json({
      message: 'This user does not have this note',
    });
  }
};

const updateUserNote = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const note = await Note.findOne({ _id: id }, '-__v');
    const { text } = req.body;
    if (!text) {
      return res.status(400).json({
        message: "Please specify 'text' parameter",
      });
    }
    note.text = text;
    const updatedNote = await note.save();
    if (!updatedNote) {
      throw new Error();
    }
    return res.json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({
      message: 'This user does not have this note',
    });
  }
};

const checkOrUncheckUserNote = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const note = await Note.findOne({ _id: id }, '-__v');
    note.completed = !note.completed;
    const updatedNote = await note.save();
    if (!updatedNote) {
      throw new Error();
    }
    return res.json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({
      message: 'This user does not have this note',
    });
  }
};

const deleteUserNote = async (req, res) => {
  const { id } = req.params;
  if (!id) {
    return res.status(400).json({
      message: "Please specify 'id' parameter",
    });
  }
  try {
    const note = await Note.findByIdAndDelete(id);
    if (!note) {
      throw new Error();
    }
    return res.json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({
      message: 'This user does not have this note',
    });
  }
};

module.exports = {
  addUserNote,
  getUserNotes,
  getUserNote,
  updateUserNote,
  checkOrUncheckUserNote,
  deleteUserNote,
};
